
## TODO Web App

### Description:
This is a basic TODO Web App which allow users to keep track of their todo-list items. It supports following features: 
* new user registrations
* adding of new tasks, 
* viewing of all tasks
* removal of tasks.

________________________________________________________

### Technologies / architecture:

**The current application is designed as a 3-tier architecture which comprises of the following layers**

* **[Frontend:](/frontend)** The frontend server will render the HTML for the client interaction with the server. Frontend is developed using `HTML5, CSS & AngularJS-v1.2`. Frontend communicates with backend over HTTP.
* **[Backend:](/server)** The `Node.JS` based HTTP REST API server will serve users requests coming from UI. Backend communicates with DB using SQL.
* **[Persistence Layer:](/database)** `MySQL` database is used as a Persistence layers for storing details about users and their tasks.
* For deployment `docker and docker-compose` is used
________________________________________________________

### Security Considerations

#### Users password storage in DB
* Users password is not stored in plain text in DB. Instead, the password is stored in salted-hash format using SHA-512 hashing algorithm.
* A random salt of 16-bytes is generated for every different user so that the salted-hash of password is always unique for users.
* The salt along with the salted-hash of the password is stored in DB to authenticate users during log-in.

________________________________________________________

### Build & deployment


#### Pre-requisites
* Docker (tested with Docker version 18.03.1-ce)
* docker-compose (tested with docker-compose version 1.21.2)

#### Steps
* Clone the repo using `git clone https://nitesh7solanki@bitbucket.org/nitesh7solanki/todo-webapp.git`
* cd `todo-webapp`
* Run `docker-compose up -d`
This will spin up 3 docker containers.
* Open the URL [http://machine_ip:9090]() on two different browsers and use below default user details to sign-in. Please replace machine-ip with the IP address of machine where application is deployed.

_______________________________________________________
### Demo Users

**User1**

	Username: alex_gomes@example.com
	Password: 123456 
	
**User2** 

	Username: david_jones@example.com
	Password: abcdef 
________________________________________________________

### Things remaining to do:
* Use JWT token for users authorization
* Deploying the application on Kubernetes cluster for container management, scalability and high-availability
* Integration tests scripts to test e2e application
* Deploy the node.js server with SSL/TLS to prevent MITM attack