/**
 * File    : tests.js
 * Purpose : Unit tests for database relate operations using sinon to mock sql. 
 */

var chai = require('chai'),
    expect = chai.expect,
    Mysql = require('mysql'),
    sinon = require('sinon'),
    UserTableRepository = require('../app/modules/database/userTableOperations.js');
	TaskTableRepository = require('../app/modules/database/taskTableOperations.js');

describe('databaseOperationsTest', function() {
   connection = Mysql.createConnection({host: 'localhost'});
   var mysqlMock = sinon.mock(connection);
   
   describe('userTableOperationsTests', function() {
		it('generates the proper SELECT query when fetching a user with given user_id and return callback', function(done) {
       var email = 'nitesh@example.com';
       var results = [{ user_id: 1, user_password: 'fdgbf56747974hkhkj', user_salt: '746356435' }];
       var expectation = mysqlMock.expects('query')
                .withArgs('SELECT user_id, user_password, user_salt FROM users WHERE user_email = ?', [ email ])
                .callsArgWith(2, null, results);

		   UserTableRepository.fetchUser([email], function(err, rows){
			   expect(rows).equals(results);
			   done();
		   })
		}); 
		
		it('generates the proper INSERT query when adding a new user and return callback', function(done) {
       var userData = {
                user_email: 'nitesh@example.com',
                user_firstname: 'nitesh',
                user_surname: 'solanki',
                user_password: '57576ghgjhg87',
                user_salt: 'ghgt667f77h7'
            };
       var results = {fieldCount: 0, affectedRows: 1, insertId: 22, serverStatus: 2, warningCount: 0, message: '', protocol41: true, changedRows: 0 }
       var expectation = mysqlMock.expects('query')
                .withArgs('INSERT INTO users SET ?', userData)
                .callsArgWith(2, null, results);

		   UserTableRepository.addUser(userData, function(err, rows){
			   expect(rows).equals(results);
			   done();
		   })
		});
		
		
		it('generates the proper SELECT query when fetching user with given email to check if user exists and return callback', function(done) {
       var email = 'nitesh@example.com';
       var results = [{ user_id: 1, user_password: 'fdgbf56747974hkhkj', user_salt: '746356435', user_firstname: 'nitesh', user_surname: 'solanki'}];
       var expectation = mysqlMock.expects('query')
                .withArgs('SELECT * FROM users WHERE user_email = ? ', [ email ])
                .callsArgWith(2, null, results);

		   UserTableRepository.checkUserNameExists([email], function(err, rows){
			   expect(rows).equals(results);
			   done();
		   })
		});
	});  
	
	describe('taskTableOperationsTests', function() {
		
		it('generates the proper INSERT query when adding a new task and return callback', function(done) {
       var userId = '1';
	   var taskName = 'Cook Food';
       var results = {fieldCount: 0, affectedRows: 1, insertId: 22, serverStatus: 2, warningCount: 0, message: '', protocol41: true, changedRows: 0 }
       var expectation = mysqlMock.expects('query')
                .withArgs('INSERT INTO tasks (user_id, task_name) VALUES(?,?)', [ userId, taskName ])
                .callsArgWith(2, null, results);

		   TaskTableRepository.addTask([ userId, taskName ], function(err, rows){
			   expect(rows).equals(results);
			   done();
		   })
		}); 
		
		it('generates the proper SELECT query when fetching all task for a user_id and return callback', function(done) {
       var userId = '12'
       var results = [{ user_id: 1, task_id: '12', task_name: 'Cook Food' }];
       var expectation = mysqlMock.expects('query')
                .withArgs('SELECT tasks.user_id, tasks.task_id, tasks.task_name FROM tasks INNER JOIN users ON tasks.user_id = users.user_id WHERE tasks.user_id = ?', [userId])
                .callsArgWith(2, null, results);

		   TaskTableRepository.getAllTasks([userId], function(err, rows){
			   expect(rows).equals(results);
			   done();
		   })
		});
		
		
		it('generates the proper DELETE query when deleting a task with given task_id and return callback', function(done) {
       var taskId = '12';
       var results = [{ user_id: 1, user_password: 'fdgbf56747974hkhkj', user_salt: '746356435', user_firstname: 'nitesh', user_surname: 'solanki'}];
       var expectation = mysqlMock.expects('query')
                .withArgs('DELETE FROM tasks WHERE task_id = ?', [ taskId ])
                .callsArgWith(2, null, results);

		   TaskTableRepository.deleteTask([taskId], function(err, rows){
			   expect(rows).equals(results);
			   done();
		   })
		});
	});  
	
});  
   	