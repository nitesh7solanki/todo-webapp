/**
 * File    : /modules/registration/registrationPresenter.js
 * Purpose : Define all functions related to registration 
 */

var userTableDatabaseHandler = require('../database/userTableOperations.js')
var genRandomString = function(length){
    return crypto.randomBytes(Math.ceil(length/2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0,length);   /** return required number of characters */
};

var sha512 = function(password, salt){
    var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(password);
    var value = hash.digest('hex');
    return {
        salt:salt,
        passwordHash:value
    };
};

/**
 * registerEntities: REST API allows admin to register members.  
 */
 
exports.registerEntities = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    console.log('======= Register entities RES API =======');
    var firstName = req.body.firstName;
	var lastName = req.body.lastName;
    var password = req.body.password;
	var email = req.body.email;   
    var sendErrorResponse = function (err, msg) {
        result = {
            Status: 'Error',
            Data: {
                message: msg,
                Error: err
            }
        };
        res.send(result);
    }
            var salt = genRandomString(16); /** Gives us salt of length 16 */
			var passwordData = sha512(password, salt);
		
            var post = {
                user_email: email,
                user_firstname: firstName,
                user_surname: lastName,
                user_password: passwordData.passwordHash,
                user_salt: passwordData.salt
            };
            userTableDatabaseHandler.addUser(post, function (err, result) {
                if (err) {
                    sendErrorResponse(err);
                } else {
                    result = {
                        Status: 'Success',
                        Data: {
                            message: 'The registerEntities API Server is working!!!!!!!!.',
                            userId: email
                        }
                    };
					console.log(result)
                    res.send(result);
                }
            });
};

/**
 * checkUsername: REST API checks the username for uniqueness.  
 */
exports.checkUsername = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    console.log('======= CheckUsername RES API =======');
    var email = req.body.email;
    userTableDatabaseHandler.checkUserNameExists([email], function (err, rows) {
        if (rows.length == 0) {
            {
                result = {
                    Status: 'success',
                    Data: {
                        message: 'Email doesnt exists.',
                    }
                };
                res.send(result);
            }
        } else if (rows.length != 0) {
            result = {
                Status: 'error',
                Data: {
                    message: 'Email exists',
                }
            };
            res.send(result);

        }
    });
};
