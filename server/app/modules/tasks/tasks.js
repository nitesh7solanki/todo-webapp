/**
 * File    : /modules/tasks/tasks.js
 * Purpose : Define all functions related to tasks like add, get, delete 
 */

var tasksTableDatabaseHandler = require('../database/taskTableOperations.js')
exports.getAllTasks = function (req, res) {
    var sendErrorResponse = function (err, msg) {
        ErrorResponse = {
            Status: 'Error',
            Data: {
                message: msg,
                Error: err
            }
        };
        res.send(ErrorResponse);
    }
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    
    tasksTableDatabaseHandler.getAllTasks([req.params.userId], function (err, rows) {
        if (err) {
            {
                dbResult = {
                    Status: 'error',
                    Data: {
                        message: 'No Tasks found',
                        Error: err
                    }
                };
                res.send(dbResult);
            }
        } else {
			 dbResult = {
                    Status: 'success',
                    Data: {
                        message: rows  
                    }
                };
				res.send(dbResult);
		}
    });
}


exports.addTask = function (req, res) {
    var sendErrorResponse = function (err, msg) {
        ErrorResponse = {
            Status: 'Error',
            Data: {
                message: msg,
                Error: err
            }
        };
        res.send(ErrorResponse);
    }
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    console.log('======= Login RES API =======');
	var taskData = [];
	taskData.push(req.body.userId);
	taskData.push(req.body.taskName);
    tasksTableDatabaseHandler.addTask(taskData, function (err, rows) {
        if (err) {
            {
                dbResult = {
                    Status: 'error',
                    Data: {
                        message: 'Wrong Username. Try again.',
                        Error: err
                    }
                };
                res.send(dbResult);
            }
        } else {
			 dbResult = {
                    Status: 'success',
                    Data: {
                        message: rows  
                    }
                };
				res.send(dbResult);
		}
    });
}

exports.deleteTask = function (req, res) {
    var sendErrorResponse = function (err, msg) {
        ErrorResponse = {
            Status: 'Error',
            Data: {
                message: msg,
                Error: err
            }
        };
        res.send(ErrorResponse);
    }
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    
    tasksTableDatabaseHandler.deleteTask([req.body.taskId], function (err, rows) {
        if (err) {
            {
                dbResult = {
                    Status: 'error',
                    Data: {
                        message: 'No Tasks found',
                        Error: err
                    }
                };
                res.send(dbResult);
            }
        } else {
			 dbResult = {
                    Status: 'success',
                    Data: {
                        message: rows  
                    }
                };
				res.send(dbResult);
		}
    });
}
