/**
 * File    : /modules/database/taskTableOperations.js
 * Purpose : Define all sql functions related to tasks table in todo-db 
 */
 
exports.addTask = function (params, callback) {
    connection.query('INSERT INTO tasks (user_id, task_name) VALUES(?,?)', params, function (err, rows) {
		callback(err, rows);
  });
}

exports.getAllTasks = function (params, callback) {
    connection.query('SELECT tasks.user_id, tasks.task_id, tasks.task_name FROM tasks INNER JOIN users ON tasks.user_id = users.user_id WHERE tasks.user_id = ?', params, function (err, rows) {
		callback(err, rows);
  });
}

exports.deleteTask = function (params, callback) {
    connection.query('DELETE FROM tasks WHERE task_id = ?', params, function (err, rows) {
		callback(err, rows);
  });
}
