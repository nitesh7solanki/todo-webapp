/**
 * File    : /modules/database/userTableOperations.js
 * Purpose : Define all sql functions related to users table in todo-db 
 */
 
exports.fetchUser = function (params, callback) {
    connection.query('SELECT user_id, user_password, user_salt FROM users WHERE user_email = ?', params, function (err, rows) {
		callback(err, rows);
  });
}

exports.addUser = function (params, callback) {
    connection.query('INSERT INTO users SET ?', params, function (err, rows) {
		callback(err, rows);
  });
}

exports.checkUserNameExists = function (params, callback) {
    connection.query('SELECT * FROM users WHERE user_email = ? ', params, function (err, rows) {
		callback(err, rows);
  });
}
