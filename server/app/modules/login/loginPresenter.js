/**
 * File    : /modules/login/loginPresenter.js
 * Purpose : Define all functions related to login 
 */


/**
 * login: REST API allows user to login to the application with the credentials  
 */
 
var userTableDatabaseHandler = require('../database/userTableOperations.js')
 var sha512 = function(password, salt){
    var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(password);
    var value = hash.digest('hex');
    return {
        salt:salt,
        passwordHash:value
    };
};

exports.login = function (req, res) {
    var sendErrorResponse = function (err, msg) {
        ErrorResponse = {
            Status: 'Error',
            Data: {
                message: msg,
                Error: err
            }
        };
        res.send(ErrorResponse);
    }
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    console.log('======= Login RES API =======');
    var enrollmentId = req.body.enrollmentId;
    var password = req.body.password;
    userTableDatabaseHandler.fetchUser([enrollmentId], function (err, rows) {
        if (err || rows.length == 0) {
            {
                dbResult = {
                    Status: 'error',
                    Data: {
                        message: 'Wrong Username. Try again.',
                        Error: err
                    }
                };
                res.send(dbResult);
            }
        } else if (rows.length != 0) {
			console.log(rows)
            var passwordHash = rows[0].user_password;
			var salt = rows[0].user_salt
			var passwordData = sha512(password, salt);
			if (passwordData.passwordHash != passwordHash) {
				dbResult = {
						Status: 'error',
						Data: {
							message: 'Wrong Password. Try again.',
							Error: err
						}
					};
					res.send(dbResult);
			}
             else {	 
		        LoginSuccess = {
                    Status: 'success',
                    Data: {
                        message: 'Login success.',
                        userId: rows[0].user_id
                    }
                };
                res.send(LoginSuccess)
           }
       };
  });
}
