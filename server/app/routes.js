/**
 * File    : /server/app/routes.js
 * Purpose : Define the URL routes 
 */

module.exports = function (app) {
   
    /**
     * Add all login related API calls. 
     * "var login" is a Object of loginPresenter.js
     */
    var login = require('./modules/login/loginPresenter.js');
    app.post('/login', login.login);
  
	 /**
     * Add all todo-task related API calls. 
     * "var task" is a Object of tasks.js
     */
	var task = require('./modules/tasks/tasks.js');
    app.post('/addTask', task.addTask);
    app.get('/getAllTasks/:userId', task.getAllTasks);
	app.post('/deleteTask', task.deleteTask);
	
    /**
     * Add all registration related API calls. 
     * "var registration" is a Object of registrationPresenter.js
     */
    var registration = require('./modules/registration/registrationPresenter.js');
    app.post('/registerEntities', registration.registerEntities);
    app.post('/checkUsername', registration.checkUsername);
};