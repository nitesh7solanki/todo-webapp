/**
 * File    : /server/database-connctor.js
 * Purpose : Define the database connector routes 
 */
var mysql = require('mysql');
module.exports = function (app) {
	// TODO: move to config file
    var db_config = {
        host: process.env.DATABASE_HOST || '127.0.0.1',
        user: 'root',
        password: '',
        port: '3306',
        database: 'todo'
    };
    connection = null;

    function handleDisconnect() {
        connection = mysql.createConnection(db_config);
        connection.connect(function (err) {
            if (err) {
                console.log('Error when connecting to db:', err);
                setTimeout(handleDisconnect, 2000);
            }
        });
        connection.on('error', function (err) {
            console.log('Database error', err);
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                handleDisconnect();
            } else {
                throw err;
            }
        });
    }
    handleDisconnect();
}