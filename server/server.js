/**
 * File    : server.js
 * Purpose : Start HTTP server at specified port. 
 */
var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
crypto = require('crypto');
var app = express();
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
    next();
});

var port = process.env.PORT || 7071;
require('./app/routes.js')(app);
require('./database-connector.js')(app);
app.listen(port);
console.log('Rest Server listening on port ' + port);
