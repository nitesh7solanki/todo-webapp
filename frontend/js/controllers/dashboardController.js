/**
 * @file 
 * Controller for Dashboard page
 * 
 */

var TODO = angular.module('TODO');

TODO.controller('dashboardController',['$window','$scope', '$cookieStore', '$rootScope', '$location', '$http','$routeParams' ,function($window, $scope, $cookieStore, $rootScope, $location, $http, $routeParams){
 $scope.userId = $cookieStore.get('globals');
 if ($scope.userId == undefined)
 {
	 $location.path('/login')
 }
$http.get(apiUrl + "getAllTasks/"+$scope.userId)
          .success(function (response) {
		           $scope.todoList = response.Data.message;
          });
						
$scope.todoAdd = function() {
		$http.post(apiUrl + "addTask", {
                            taskName: $scope.todoInput,
							userId: $scope.userId
                        })
                        .success(function (response) {
							$scope.todoList.push({task_name:$scope.todoInput, user_id:$scope.userId, task_id:response.Data.message.insertId});
							$scope.todoInput = "";
                    });
 };
	
	
$scope.deleteItem = function(task, index) {
		var oldList = $scope.todoList 
		 $http.post(apiUrl + "deleteTask", {
							taskId: task.task_id
                    })
                      .success(function (response) {
					  $scope.todoList.splice(index, 1);				
        });
 };

$scope.logOut = function () {
        $rootScope.globals = {};
        $cookieStore.remove('globals');
		$location.path('/login');
  };	
	
	
}]);


