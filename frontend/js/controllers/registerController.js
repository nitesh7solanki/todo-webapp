/**
 * @file 
 * Controller for register page
 * 
 */
angular.module('Registration')
    .controller('registerController', ['$scope', '$rootScope', '$location', '$http',
        function ($scope, $rootScope, $location, $http) {
            $scope.emailChange = function () {
				$scope.emailExist = false;
                $http.post(apiUrl + "checkUsername", {
                        email: $scope.user.email
                    })
                    .success(function (response) {
                        if (response.Status == "success") {
                            $scope.enrollmentIdMsg = "Username available";
                            $scope.emailStyle = {
                                color: 'green'
                            };
                        } else {
                            $scope.enrollmentIdMsg = "Username not available";
                            $scope.emailStyle = {
                                color: 'red'
                            };
							$scope.emailExist = true;
                        }
                    });
            }
          
            //register user function
            $scope.registerUser = function () {
                $("#modal").show();
                $http.post(apiUrl + "registerEntities", $scope.user)
                    .success(function (response) {
                        $("#modal").hide();
                        if (response.Status == "Error") {
                            $scope.error = true;
                            $scope.errorMsg = response.Data.message;
                            $scope.registered = false;
                            $scope.dataLoading = false;
                        } else {
                            $scope.register = "Registred successfully.";
                            $scope.registered = true;
                            $location.path('/login'); // redirect to login
                        }
                    });
            }
        }]);