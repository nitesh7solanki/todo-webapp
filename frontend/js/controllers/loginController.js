/**
 * @file 
 * Controller for Login page
 * 
 */
angular.module('Authentication')
    .controller('loginController', ['$scope', '$cookieStore', '$rootScope', '$location', 'AuthenticationService', '$http', '$routeParams',
        function ($scope, $cookieStore, $rootScope, $location, AuthenticationService, $http, $routeParams) {
            $scope.token = $routeParams.Token;
            // reset login status
            AuthenticationService.ClearCredentials();
            // Register method
            $scope.register = function () {
                $scope.dataLoading = true;
                $location.path('/register');
            };
            $scope.login = function () {
                $scope.dataLoading = true;
                AuthenticationService.Login($scope.username, $scope.password, function (response) {
                    if (response.success) {
						$cookieStore.put('globals', response.Data.userId);
						$location.path('/dashboard');
						
                    } else {
                        $scope.error = true;
                        $scope.errorMsg = response.Data.message;
                        $scope.dataLoading = false;
                    }
                });
            };
            
        }]);