angular.module('Authentication')
    .factory('AuthenticationService', ['$http', '$cookieStore', '$rootScope', '$timeout', '$location',
        function ($http, $cookieStore, $rootScope, $timeout, $location) {
            var service = {};
            var userType = "";

            service.Login = function (username, password, callback) {
                $http.post(apiUrl + 'login', {
                        enrollmentId: username,
                        password: password
                    })
                    .success(function (response) {
                        if (response.Status == "error") {
                            response.message = 'Username or password is incorrect';
                        } else
                            response.success = true;
							callback(response);
                    });
            };
			
			
            service.ClearCredentials = function () {
                $rootScope.globals = {};
                $cookieStore.remove('globals');
            };
			
            return service;
        }])