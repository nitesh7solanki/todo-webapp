// define our application and pull in ngRoute and ngAnimate
angular.module('Authentication', []);
angular.module('Registration', []);

var TODO = angular.module('TODO', ['ngRoute', 'ngCookies', 'Authentication', 'Registration'])
    .run(['$rootScope', '$location', '$cookieStore', '$http', '$interval', '$timeout',
        function ($rootScope, $location, $cookieStore, $http, $interval, $timeout) {
           
            // keep user logged in after page refresh
            $rootScope.globals = $cookieStore.get('globals') || {};
            if (!$rootScope.globals.currentUser) {
                $rootScope.globals.currentUser = '';
            }
            
        }]);

TODO.config(function ($routeProvider) {
    $routeProvider
    // Login Page
        .when('/login', {
        templateUrl: 'partials/login.html',
        controller: 'loginController',
    })

    // Logout Page
    .when('/logout', {
        redirectTo: '/login'
    })

    // Dashboard Page
    .when('/dashboard', {
        templateUrl: 'partials/dashboard.html',
        controller: 'dashboardController'
    })

   
    // Register Page
    .when('/register', {
        templateUrl: 'partials/register.html',
        controller: 'registerController',
    })

    // Default Page after login
    .otherwise({
        redirectTo: '/dashboard'
    });
});